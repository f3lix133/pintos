/* Felix: Fixed point numbers are in signed p.q format, where p+q=31,
 *  and f is 1<<q. Here we use 11.20 as p.q, so f=1<<20=1048576.
 * float x,y;
 * int n; */
#define FLOAT_F 1048576

/* convert n to fixed point */
#define INT_TO_FLOAT(n) (n*FLOAT_F)

/* convert x to integer (rounding toward zero)*/
#define FLOAT_TO_INT_ZERO(x) (x/FLOAT_F)

/* convert x to integer (rounding to nearest) */
#define FLOAT_TO_INT_NEAR(x) (x>=0?((x+FLOAT_F/2)/FLOAT_F):((x-FLOAT_F/2)/FLOAT_F))

/* add x and n:*/
#define FLOAT_ADD_INT(x,n) (x+n*FLOAT_F)

/* subtract n from x:*/
#define FLOAT_SUB_INT(x,n) (x-n*FLOAT_F) 

/* multiply x by y:*/
#define FLOAT_MULTI(x,y) (((int64_t)x)*y/FLOAT_F)

/* multiply x by n:*/
#define FLOAT_MULTI_INT(x,n) (x*n)

/* divide x by y:*/
#define FLOAT_DIV(x,y) (((int64_t)x)*FLOAT_F/y)

/* divide x by n:*/
#define FLOAT_DIV_INT(x,n) (x/n)





 





