<h1>This is README for the pintos project.</h1>
<h3>Copyright by Felix Lee &copy; 2011 East China Normal University</h3>
<h3>Email: leegyao@gmail.com</h3>          
<br/>
<p>Currently I only unleashed the implementation on project 1, aka the <strong>Threads</strong> part of pintos.</p>
<p>As you already know, pintos was not a project start from scratch, it has some basic infrastructure to consist of a basic OS kernel.</p>
<p>All the codes and scripts can be found under the folder <strong>src</strong>.</p>
<p>To figure out the codes written by Felix in detail, please refer to <strong>changelog.diff</strong>.</p>
<p>To read the official lab manual released by Stanford, please refer to <strong>pintos.pdf</strong>.</p>
<p>To realize the design aspect of this project, please refer to <strong>Felix-pintos-project1.pdf</strong>.</p>
<br/>
<pre>
+++++++++++++++++++++++++++++++++++++++++++++++
++     Code modify statistics by diffstat    ++
+++++++++++++++++++++++++++++++++++++++++++++++

 devices/timer.c       |   10 +
 threads/fixed-point.h |   43 +++++++
 threads/init.c        |    5 
 threads/synch.c       |   95 ++++++++++++++--
 threads/synch.h       |   17 ++
 threads/thread.c      |  292 ++++++++++++++++++++++++++++++++++++++++++++++++--
 threads/thread.h      |   34 +++++
 7 files changed, 476 insertions(+), 20 deletions(-)
</pre>
<br/>
<pre>
+++++++++++++++++++++++++++++++++++++++++++++++++
++ For more infomation, pls see changelog.diff. +
++ /pintos/changelog.diff                       +
+++++++++++++++++++++++++++++++++++++++++++++++++

++++++++++++++++++++++++++++++++++++++++++++++++
++             make check result              ++
++++++++++++++++++++++++++++++++++++++++++++++++
pass tests/threads/mlfqs-block
pass tests/threads/alarm-single
pass tests/threads/alarm-multiple
pass tests/threads/alarm-simultaneous
pass tests/threads/alarm-priority
pass tests/threads/alarm-zero
pass tests/threads/alarm-negative
pass tests/threads/priority-change
pass tests/threads/priority-donate-one
pass tests/threads/priority-donate-multiple
pass tests/threads/priority-donate-multiple2
pass tests/threads/priority-donate-nest
pass tests/threads/priority-donate-sema
pass tests/threads/priority-donate-lower
pass tests/threads/priority-fifo
pass tests/threads/priority-preempt
pass tests/threads/priority-sema
pass tests/threads/priority-condvar
pass tests/threads/priority-donate-chain
pass tests/threads/mlfqs-load-1
pass tests/threads/mlfqs-load-60
pass tests/threads/mlfqs-load-avg
pass tests/threads/mlfqs-recent-1
pass tests/threads/mlfqs-fair-2
pass tests/threads/mlfqs-fair-20
pass tests/threads/mlfqs-nice-2
pass tests/threads/mlfqs-nice-10
pass tests/threads/mlfqs-block
All 27 tests passed.
++++++++++++++++++++++++++++++++++++++++++++++++
</pre>
